using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.Media.Imaging;
using Avalonia.Controls;
using System;
using Avalonia.Input;

namespace spriteMove.Views
{
    public partial class MainWindow : Window
    {
        private Image sprite = new Image();
        private double spriteTopPosition = 0;

        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            sprite = this.FindControl<Image>("Sprite") ?? throw new InvalidOperationException("Sprite não encontrado.");
            sprite.Source = new Bitmap("Assets/sans.png");

            this.KeyDown += HandleKeyDown;
        }

        private void HandleKeyDown(object? sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                MoveSprite(-10);
            }
            else if (e.Key == Key.Down)
            {
                MoveSprite(10);
            }
        }

        private void MoveSprite(int deltaY)
        {
            spriteTopPosition += deltaY;
            sprite.Margin = new Thickness(0, spriteTopPosition, 0, 0);
        }
    }
}